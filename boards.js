const board = $('.board')[0];
const table = document.createElement('table');
table.classList.add('board-table');
const numRows = 16;
const numCols = 30;
for (let i = 0; i < numRows; i++) {
    const row = document.createElement('tr');
    row.id = `row${i}`;
    row.classList.add('row');
    for (let j = 0; j < numCols; j++) {
        const col = document.createElement('td');
        col.id = `col${i}${j}`;
        col.classList.add('col');
        const chbx = document.createElement('input');
        chbx.id = `chbx${i}-${j}`;
        chbx.name = `chbx${i}${j}`;
        chbx.type = 'checkbox';
        chbx.classList.add('checkbx', 'hidden');
        const lbl = document.createElement('label');
        lbl.classList.add('lbl');
        lbl.htmlFor = `chbx${i}${j}`;

        lbl.onclick = (e) => {
            const self = e.target;
            if (self.classList.contains('lbl')) {
                const isChecked = self.classList.contains('lbl-checked');
                isChecked
                    ? self.classList.remove('lbl-checked')
                    : self.classList.add('lbl-checked');
            }
        };
        lbl.appendChild(chbx);
        col.appendChild(lbl);
        row.appendChild(col);
    }
    table.appendChild(row);
}
board.appendChild(table);
