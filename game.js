const getLivingCells = () => {
    return $('.lbl-checked');
};

const getDeadCells = (living) => {
    const all = $('.lbl');
    return Array.from(all).filter((e) => Array.from(living).indexOf(e) === -1);
};

const getRowAndCol = (cell) => {
    const cellId = cell.firstChild.id.match(/[0-9]+\-[0-9]+/)[0];
    return [cellId.split('-')[0], cellId.split('-')[1]];
};

const checkTopLeft = (row, col) => {
    if (row != 0) {
        if (col != 0) {
            const cell = $(`#chbx${--row}-${--col}`);
            return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
        }
        return 0;
    }
    return 0;
};

const checkTop = (row, col) => {
    if (row != 0) {
        const cell = $(`#chbx${--row}-${col}`);
        return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
    }
    return 0;
};

const checkTopRight = (row, col) => {
    if (row != 0) {
        if (col != 19) {
            const cell = $(`#chbx${--row}-${++col}`);
            return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
        }
        return 0;
    }
    return 0;
};

const checkLeft = (row, col) => {
    if (col != 0) {
        const cell = $(`#chbx${row}-${--col}`);
        return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
    }
    return 0;
};

const checkRight = (row, col) => {
    if (col != 29) {
        const cell = $(`#chbx${row}-${++col}`);
        return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
    }
    return 0;
};

const checkBottomLeft = (row, col) => {
    if (row != 15) {
        if (col != 0) {
            const cell = $(`#chbx${++row}-${--col}`);
            return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
        }
        return 0;
    }
    return 0;
};

const checkBottom = (row, col) => {
    if (row != 15) {
        const cell = $(`#chbx${++row}-${col}`);
        return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
    }
    return 0;
};

const checkBottomRight = (row, col) => {
    if (row != 15) {
        if (col != 29) {
            const cell = $(`#chbx${++row}-${++col}`);
            return cell?.parentNode?.classList.contains('lbl-checked') ? 1 : 0;
        }
        return 0;
    }
    return 0;
};

const checkNeighbors = (cell) => {
    const [row, col] = getRowAndCol(cell);
    const additionReducer = (prev, curr) => prev + curr;
    const neighbors = [
        checkTopLeft(row, col),
        checkTop(row, col),
        checkTopRight(row, col),
        checkLeft(row, col),
        checkRight(row, col),
        checkBottomLeft(row, col),
        checkBottom(row, col),
        checkBottomRight(row, col),
    ];
    let livingNeighbors = 0;
    livingNeighbors = neighbors.reduce(additionReducer, livingNeighbors);
    return livingNeighbors;
};

const stillLiving = (living) => {
    const stillLiving = [];
    Array.from(living).forEach((cell) => {
        const livingNeighbors = checkNeighbors(cell);

        if (livingNeighbors == 2 || livingNeighbors == 3) {
            stillLiving.push(cell);
        }
    });
    return stillLiving;
};

const birthNewCells = (dead) => {
    const newCellsToBirth = [];
    Array.from(dead).forEach((cell) => {
        const livingNeighbors = checkNeighbors(cell);

        if (livingNeighbors == 3) {
            newCellsToBirth.push(cell);
        }
    });
    return newCellsToBirth;
};

const resetBoard = () => {
    Array.from(document.getElementsByTagName('label')).forEach((l) => {
        l.classList.remove('lbl-checked');
    });
};

const redrawBoard = (newCells) => {
    resetBoard();
    Array.from(newCells).forEach((c) => {
        const [row, col] = getRowAndCol(c);
        const cell = $(`#chbx${row}-${col}`).parentNode;
        cell.click();
    });
};
let playing = false;
const playGame = () => {
    playing = true;
    const living = getLivingCells();
    const dead = getDeadCells(living);
    const newCells = stillLiving(living).concat(birthNewCells(dead));
    redrawBoard(newCells);

    setTimeout(() => {
        if (playing) {
            playGame();
        }
    }, 900);
};

const stopGame = () => {
    playing = false;
};